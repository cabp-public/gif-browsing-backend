<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    protected $table;
    protected $primaryKey;
    protected $fillable;
    protected $hidden = [];
    protected $prefix;
    protected $logger;

    function __construct(array $attributes = [])
    {
        $this->logger = app('Psr\Log\LoggerInterface');

        parent::__construct($attributes);
    }

    protected function catchException(\Exception $exception, bool $exit = true)
    {
        try {
            if (app()->environment('local')) {
                echo $exception->getMessage();

                if ($exit) {
                    exit;
                }
            }
            else {
                $this->logger->error($exception, ['exception' => $exception]);
            }
        }
        catch (\Exception $e) {
            throw $exception;
        }
    }

    public function exists(array $params): bool
    {
        $output = false;

        try {
            $total = self::where($params[0], $params[1])->count();
            $output = $total > 0;
        }
        catch (\Exception $exception) {
            $this->catchException($exception);
        }
        finally {
            return $output;
        }
    }
}