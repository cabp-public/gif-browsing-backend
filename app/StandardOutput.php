<?php

namespace App;

class StandardOutput
{
    public $success = false;
    public $message = '';
    public $data = [];
}
