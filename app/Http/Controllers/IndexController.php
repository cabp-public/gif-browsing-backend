<?php

namespace App\Http\Controllers;

use App\Models\ImageModel;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;

final class IndexController extends Controller
{
    const FILES_PATH = 'FILES_PATH';

    /**
     * IndexController constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        parent::__construct($logger);
    }

    /**
     * Default index action
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $status = 400;
        $output = [
            'success' => false,
            'details' => [],
        ];

        try {
            $status = 200;
            $output = [
                'success' => true,
                'details' => "Hello...",
            ];
        }
        catch (\Exception $exception) {
            $this->catchException($exception, $status, $output);
        }
        finally {
            return $this->respond($output, $status);
        }
    }

    public function view(Request $request, int $imgId)
    {
        $status = 400;
        $output = [
            'success' => false,
            'details' => 'Image not found',
        ];

        try {
            $imageCount = ImageModel::where('img_id', $imgId)->count();

            if ($imageCount === 1) {
                $filePath = env(self::FILES_PATH) . $imgId . '.gif';
                $status = 200;
                return $this->respondImageGif($filePath, $status);
            }
            else {
                return $this->respond($output, $status);
            }
        }
        catch (\Exception $exception) {
            $this->catchException($exception, $status, $output);
        }
    }
}
