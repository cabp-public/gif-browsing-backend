<?php

namespace App\Http\Controllers;

use App\Models\ImageModel;
use Illuminate\Support\Facades\DB;
use Psr\Log\LoggerInterface;
use Illuminate\Http\Request;
use App\Services\GifSearch;
use App\Models\UserModel;

final class UserController extends Controller
{
    /**
     * SearchController constructor.
     * @param LoggerInterface $logger
     * @param GifSearch $gifSearch
     */
    public function __construct(LoggerInterface $logger, GifSearch $gifSearch)
    {
        parent::__construct($logger);
    }

    public function register(Request $request)
    {
        $status = 400;
        $output = self::DEFAULT_HTTP_RESPONSE_OUTPUT;

        try {
            $input = json_decode($request->getContent(), true);
            $found = UserModel::where('user_email', $input['email'])->first();

            if (is_null($found)) {
                $user = new UserModel([
                    'user_email' => $input['email'],
                    'user_password' => $input['password'],
                ]);
                $user->save();

                $status = 200;
                $output['status'] = $status;
                $output['success'] = true;
                $output['message'] = 'Done';
            }
            else {
                $status = 409;
                $output['status'] = $status;
                $output['success'] = false;
                $output['message'] = 'User already exists';
            }
        }
        catch (\Exception $exception) {
            $this->catchException($exception, $status, $output);
        }
        finally {
            return $this->respond($output, $status);
        }
    }

    public function favorite(Request $request)
    {
        $status = 400;
        $output = self::DEFAULT_HTTP_RESPONSE_OUTPUT;

        try {
            $input = json_decode($request->getContent(), true);
            $user = UserModel::where('user_email', $input['email'])->first();
            $imageExists = ImageModel::where('img_id', $input['image'])->count();
            $favoriteExists = DB::table('user_favourites')
                ->where('img_id', '=', $input['image'])
                ->where('user_id', '=', $user->user_id)
                ->count();

            if (!is_null($user) && $imageExists) {
                if ($favoriteExists === 0) {
                    DB::table('user_favourites')->insert([
                        'img_id' => $input['image'],
                        'user_id' => $user->user_id,
                    ]);

                    $status = 200;
                    $output['status'] = $status;
                    $output['success'] = true;
                    $output['message'] = 'Done';
                }
                else {
                    $status = 409;
                    $output['status'] = $status;
                    $output['success'] = false;
                    $output['message'] = 'Favorite already exists';
                }
            }
            else {
                $status = 404;
                $output['status'] = $status;
                $output['success'] = false;
                $output['message'] = 'User or image do not exist';
            }
        }
        catch (\Exception $exception) {
            $this->catchException($exception, $status, $output);
        }
        finally {
            return $this->respond($output, $status);
        }
    }
}
