<?php

namespace App\Http\Controllers;

use Psr\Log\LoggerInterface;
use Illuminate\Http\Request;
use App\Services\GifSearch;

final class SearchController extends Controller
{
    private $gifSearch;

    /**
     * SearchController constructor.
     * @param LoggerInterface $logger
     * @param GifSearch $gifSearch
     */
    public function __construct(LoggerInterface $logger, GifSearch $gifSearch)
    {
        $this->gifSearch = $gifSearch;

        parent::__construct($logger);
    }

    public function searchByTag(Request $request)
    {
        $status = 400;
        $output = self::DEFAULT_HTTP_RESPONSE_OUTPUT;

        try {
            // var_dump($request->getMethod());
            // exit;
            $input = json_decode($request->getContent(), true);
            $searchResults = $this->gifSearch->byTag($input);

            $status = 200;
            $output['status'] = $status;
            $output['success'] = true;
            $output['message'] = 'Done';
            $output['data'] = $searchResults->data;
        }
        catch (\Exception $exception) {
            $this->catchException($exception, $status, $output);
        }
        finally {
            return $this->respond($output, $status);
        }
    }

    public function getHistory(Request $request, int $page)
    {
        $status = 400;
        $output = self::DEFAULT_HTTP_RESPONSE_OUTPUT;

        try {
            $entries = $this->gifSearch->getHistory($page);

            $status = 200;
            $output['status'] = $status;
            $output['success'] = true;
            $output['message'] = 'Done';
            $output['data'] = $entries;
        }
        catch (\Exception $exception) {
            $this->catchException($exception, $status, $output);
        }
        finally {
            return $this->respond($output, $status);
        }
    }
}
