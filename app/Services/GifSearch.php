<?php

namespace App\Services;

use App\Models\HistoryModel;
use App\Models\ImageModel;
use App\StandardOutput;

class GifSearch extends Service
{
    private $page;

    public function __construct()
    {
        parent::__construct();
    }

    private function getTotalResults(array $keywords)
    {
        $output = 0;

        try {
            // $output = ImageModel::where('img_tags', 'LIKE', '%java%')->count();

            foreach ($keywords as $keyword) {
                $output += ImageModel::where('img_tags', 'LIKE', '%' . $keyword . '%')->count();
            }
        }
        catch (\Exception $exception) {
            $this->catchException($exception);
        }
        finally {
            return $output;
        }
    }

    private function getPageResults(int $page, int $imit, array $keywords)
    {
        $output = [];

        try {
            $offset = $page * $imit;

            foreach ($keywords as $keyword) {
                $images = ImageModel::where('img_tags', 'LIKE', '%' . $keyword . '%')
                    ->offset($offset)->limit($imit)->get();

                if (count($images)) {
                    foreach ($images as $image) {
                        $output[] = [
                            'key' => time() . $image->img_id,
                            'viewUrl' => '/view/' . $image->img_id,
                            'tags' => explode(',', $image->img_tags)
                        ];
                    }
                }
            }
        }
        catch (\Exception $exception) {
            $this->catchException($exception);
        }
        finally {
            return $output;
        }
    }

    public function byTag(array $input): StandardOutput
    {
        $output = new StandardOutput();

        try {
            $this->page = $input['page'] - 1;

            // History
            $historyEntry = new HistoryModel([
                'his_search_string' => implode(',', $input['keywords']),
            ]);

            $historyEntry->save();

            // Get results (workaround)
            $totalResults = $this->getTotalResults($input['keywords']);
            $diff = $totalResults % $input['pageLimit'];
            $totalPages  = round($totalResults / $input['pageLimit']);
            $totalPages += $diff > 0 ? 1 : 0;
            $totalPages = $totalPages < 1 ? 1 : $totalPages;

            $pageResults = $this->getPageResults(
                $this->page,
                $input['pageLimit'],
                $input['keywords']
            );

            $output->success = true;
            $output->data = [
                'totalPages' => $totalPages,
                'totalResults' => $totalResults,
                'currentPage' => $input['page'],
                'results' => $pageResults,
            ];
        }
        catch (\Exception $exception) {
            $this->catchException($exception);
        }
        finally {
            return $output;
        }
    }

    public function getTotalSearchRequests(): int
    {
        $output = 0;

        try {
            $output = HistoryModel::select('his_id')->count();
        }
        catch (\Exception $exception) {
            $this->catchException($exception);
        }
        finally {
            return $output;
        }
    }

    public function getHistory(int $page, int $limit = 5): array
    {
        $output = [];

        try {
            $offset = $page * $limit;
            $count = HistoryModel::select('his_id')->count();

            $entries = HistoryModel::select('*')
                ->offset($offset)
                ->limit($limit)
                ->get()
                ->toArray();

            $output = [
                'currentPage' => $page,
                'totalPages' => round($count / $limit),
                'pageResults' => $entries,
            ];
        }
        catch (\Exception $exception) {
            $this->catchException($exception);
        }
        finally {
            return $output;
        }
    }
}
