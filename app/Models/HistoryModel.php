<?php

namespace App\Models;

use App\Entity;

/**
 * @property integer $his_id'
 * @property string $his_search_string
 * @property string $his_created_at
 */
class HistoryModel extends Entity
{
    const CREATED_AT = 'his_created_at';
    const UPDATED_AT = null;

    protected $table = 'history';
    protected $primaryKey = 'his_id';
    protected $fillable = ['his_search_string'];

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}