<?php

namespace App\Models;

use App\Entity;

/**
 * @property integer $img_id'
 * @property string $img_created_at
 * @property string $img_updated_at
 */
class ImageModel extends Entity
{
    protected $table = 'images';
    protected $primaryKey = 'img_id';
    protected $fillable = [];

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function tags()
    {
        return $this->belongsToMany(
            TagModel::class,
            'image_tags',
            'tag_id',
            'img_id'
        );
    }
}