<?php

namespace App\Models;

use App\Entity;

/**
 * @property integer $tag_id'
 * @property string $tag_created_at
 * @property string $tag_updated_at
 */
class TagModel extends Entity
{
    protected $table = 'tags';
    protected $primaryKey = 'tag_id';
    protected $fillable = [
        'tag_word'
    ];

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function images()
    {
        return $this->belongsToMany(
            'App\Models\ImageModel',
            'image_tags',
            'tag_id',
            'img_id'
        );
    }
}