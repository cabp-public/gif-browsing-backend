<?php

namespace App\Models;

use App\Entity;

/**
 * @property integer $user_id'
 * @property string $user_created_at
 * @property string $user_updated_at
 */
class UserModel extends Entity
{
    const CREATED_AT = 'user_created_at';
    const UPDATED_AT = 'user_updated_at';

    protected $table = 'users';
    protected $primaryKey = 'user_id';
    protected $fillable = [
        'user_email',
        'user_password',
    ];

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
