<?php

// Default
$router->get('/', 'IndexController@index');

$router->get('/history/{page}', 'SearchController@getHistory');
$router->get('/view/{imgId}', 'IndexController@view');
$router->post('/register', 'UserController@register');
$router->post('/favorite', 'UserController@favorite');
$router->post('/search', 'SearchController@searchByTag');
