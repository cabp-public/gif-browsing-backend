/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     02/08/2019 04:04:31                          */
/*==============================================================*/

drop table if exists history;
drop table if exists image_tags;
drop table if exists images;
drop table if exists tags;
drop table if exists user_favourites;
drop table if exists users;

/* Table: history                                               */
create table history
(
   his_id               int not null auto_increment,
   his_search_string    varchar(100) not null,
   his_created_at       datetime not null,
   primary key (his_id)
);

/* Table: image_tags                                            */
create table image_tags
(
   tag_id               int not null,
   img_id               int not null,
   primary key (tag_id, img_id)
);

/* Table: images                                                */
create table images
(
   img_id               int not null auto_increment,
   img_created_at       datetime,
   img_updated_at       datetime,
   primary key (img_id)
);

/* Table: tags                                                  */
create table tags
(
   tag_id               int not null auto_increment,
   tag_word             varchar(64),
   tag_created_at       datetime,
   tag_updated_at       datetime,
   primary key (tag_id)
);

/* Table: user_favourites                                       */
create table user_favourites
(
   img_id               int not null,
   user_id              int not null,
   primary key (img_id, user_id)
);

/* Table: users                                                 */
create table users
(
   user_id              int not null auto_increment,
   user_email           varchar(100) not null,
   user_created_at      datetime,
   user_updated_at      datetime,
   primary key (user_id)
);

alter table image_tags add constraint fk_image_tags foreign key (tag_id)
      references tags (tag_id) on delete restrict on update restrict;

alter table image_tags add constraint fk_image_tags2 foreign key (img_id)
      references images (img_id) on delete restrict on update restrict;

alter table user_favourites add constraint fk_user_favourites foreign key (img_id)
      references images (img_id) on delete restrict on update restrict;

alter table user_favourites add constraint fk_user_favourites2 foreign key (user_id)
      references users (user_id) on delete restrict on update restrict;

