-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.15 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table gif_browser.images: ~3 rows (approximately)
DELETE FROM `images`;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`img_id`, `img_created_at`, `img_updated_at`, `img_tags`) VALUES
	(1, '2019-08-02 04:05:50', NULL, 'java,code,coffee'),
	(2, '2019-08-02 04:06:06', NULL, 'pink,panther,ups'),
	(3, '2019-08-02 04:06:11', NULL, 'programming,developer'),
	(4, '2019-08-05 03:03:56', NULL, 'mail'),
	(5, '2019-08-05 03:04:03', NULL, 'java,coffee'),
	(6, '2019-08-05 03:04:05', NULL, 'java,coffee'),
	(7, '2019-08-05 03:04:07', NULL, 'java,coffee'),
	(8, '2019-08-05 03:18:26', NULL, 'coffee'),
	(9, '2019-08-05 03:18:33', NULL, 'coffee'),
	(10, '2019-08-05 03:18:47', NULL, 'coffee'),
	(11, '2019-08-05 03:18:58', NULL, 'coffee');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Dumping data for table gif_browser.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `user_email`, `user_created_at`, `user_updated_at`, `user_password`) VALUES
	(1, 'uncle_ben@hotmail.com', '2019-08-02 18:27:38', NULL, '1234');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table gif_browser.user_favourites: ~0 rows (approximately)
DELETE FROM `user_favourites`;
/*!40000 ALTER TABLE `user_favourites` DISABLE KEYS */;
INSERT INTO `user_favourites` (`img_id`, `user_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `user_favourites` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
