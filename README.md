# GIF Browsing - BackEnd

BackEnd for the GIF Browsing coding demo.

## The Approach
In order to make the application maintainable, salable and testable I decided to use Behavior Driven Development as the main approach for this example.

> "Behaviour Driven Development (BDD) is a synthesis and refinement of practices stemming from Test Driven Development (TDD) and Acceptance Test Driven Development (ATDD)."

From the [Agile Alliance Glossary](https://www.agilealliance.org/glossary/bdd/)

#### Behat
It's a BDD framework for PHP (inspired by cucumber) which allows us to create `.features` using Gherkin Syntax.
> "It is a tool to support you in delivering software that matters through continuous communication, deliberate discovery and test-automation."

#### Lumen
Lumen is the famous microframework from Laravel, I'm using this one due to simple easiness. Its testing features, however, don't support BDD out of the box so you'll see an slightly different structure in the `test` folder:

* features - contains all .feature files
* tests - contains the actual testing implementation

## How to Install
To run this project you'll need a web server, the most recommended ones are [Apache](https://httpd.apache.org/) or [NGINX](https://www.nginx.com/); create the `BlueCoding` directory within your server's "document root" then clone the repository in it. On your web server, a new virtual host and point it to the `/<web_root>/BlueCoding/public` directory. To install packages the obvious choice is Composer, if you need to know how to get it and install it just click [here](https://getcomposer.org/):

    $ cd /<web_root>/MyCode
    $ composer install

#### The .env file
The install script will create a `.env` file in your project directory, you MUST update it in order to customize the database connection settings and other environment-specific values. In case you need to copy the file manually, simple rename the `.env.exaple` file to `.env`.

#### Database
In the `_resources` directory you'll find the `gif_browser.sql` file, it contains the creation script for the database. You'll also find the `data.sql` file, which contains data for testing purposes.

#### Running Test Suites
The `behat.yml` file includes settings for the `search` suite, here you can include more suites according to your needs. To run tests simply:

    $ cd /<web_root>/MyCode
    $ vendor/bin/behat

If you want to run only one scenario include the `--suite` argument:

    $ vendor/bin/behat --suite=search
    $ vendor/bin/behat --suite=user