@search
Feature: SEARCH
  In order to get a welcome message
  As a Visitor
  I need to be able to ask for the welcome message

  Scenario: Asking for welcome message
    When Consumer performs a GET request to "/"
    Then Consumer gets a 200 response

  Scenario: Search images
    Given Keywords to search are "java,ok"
    And Detailed search is off
    And Requested page is 1
    And Page limit is 5
    When Consumer requests to search GIFs using the given keywords
    Then Consumer gets a 200 response
    And There should be 1 total results
    And There should be 1 pages
    And Page 1 should have 1 results

#  Scenario: Save search
#    And Keywords to search are "java,ok"
#    When Consumer requests to search GIFs using the given keywords
#    Then Consumer gets a 200 response
#    And Total search requests should increase by 1