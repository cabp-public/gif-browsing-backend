@user
Feature: USER
  In order to save a GIF as favourite
  As a User
  I need to be able to register, start a session, save the gif and close my session

#  Scenario: Register new user
#    Given User e-mail is "john.doe@hotmail.com"
#    And User password is "1234"
#    When User tries to register
#    Then Consumer gets a 200 response

#  Scenario: Register an existent user
#    Given User e-mail is "uncle_ben@hotmail.com"
#    And User password is "1234"
#    When User tries to register
#    Then Consumer gets a 409 response

  Scenario: Save favourite gif
    Given User starts session as "uncle_ben@hotmail.com" with password "1234"
    When User favourites GIF-ID 1
    Then Consumer gets a 200 response
