<?php

/**
 * Defines application features from the API context.
 */
class SearchContext extends TestContext
{
    const URI_NOT_FUND = 'URI_NOT_FUND';
    const URI_SEARCH = 'URI_SEARCH';
    const URI_TERMS_MOST_USED = 'URI_TERMS_MOST_USED';

    private $uriNotFound;
    private $httpResponse;
    private $searchKeywords;
    private $page = 1;
    private $pageLimit = 5;
    private $totalSearchRequests = 0;
    private $detailedSearch = false;
    private $searchService;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->uriNotFound = env(self::URI_NOT_FUND, '/not-found');
        $this->httpResponse = new \Illuminate\Http\Response();

        $this->searchService = $this->app->make('App\Services\GifSearch');
        $this->totalSearchRequests = $this->searchService->getTotalSearchRequests();
    }

    /**
     * @return array
     */
    private function getContentData()
    {
        $output = [];

        try {
            $content = json_decode($this->httpResponse->content(), true);
            $output = $content['data'];
        }
        catch (\Exception $exception) {
        }
        finally {
            return $output;
        }
    }

    /**
     * @Given /^Keywords to search are "([^"]*)"$/
     * @param string $keywords
     */
    public function setSearchKeywords(string $keywords)
    {
        try {
            $this->searchKeywords = explode(',', $keywords);
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Detailed search is (.*)$/
     * @param string $keyword
     */
    public function toggleDetailedSearch(string $keyword)
    {
        try {
            $this->detailedSearch = intval(strtolower($keyword) === 'on');
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Requested page is (\d+)$/
     * @param int $page
     */
    public function setPage(int $page)
    {
        try {
            $this->page = $page;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Page limit is (\d+)$/
     * @param int $limit
     */
    public function setPageLimit(int $limit)
    {
        try {
            $this->pageLimit = $limit;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @When Consumer performs a GET request to :apiUrl
     * @param string $apiUri
     */
    public function consumerPerformsGetRequest(string $apiUri)
    {
        try {
            $this->httpResponse = $this->get($apiUri)->response;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }

    /**
     * @Then Consumer gets a :expectedHttpCode response
     * @param $expectedHttpCode
     */
    public function consumerGetsHttpResponse(int $expectedHttpCode)
    {
        $output = false;
        $content = [];

        try {
            $status = $this->httpResponse->status();
            $content = json_decode($this->httpResponse->content(), true);
            $output = $status === $expectedHttpCode;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
        finally {
            $message = "\nAssertion Error:\n" . json_encode($content);
            $this->assertTrue($output, $message);
        }
    }

    /**
     * @When Consumer requests to search GIFs using the given keywords
     */
    public function searchImages()
    {
        try {
            $apiUri = env(self::URI_SEARCH, $this->uriNotFound);

            $this->httpResponse = $this
                ->json('POST', $apiUri, [
                    'keywords' => $this->searchKeywords,
                    'detailed' => $this->detailedSearch,
                    'page' => $this->page,
                    'pageLimit' => $this->pageLimit,
                ])
                ->response;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }

    /**
     * @Given /^There should be (\d+) total results$/
     * @param int $totalResults
     */
    public function totalResultsShouldBe(int $totalResults)
    {
        $output = false;
        $data = [];

        try {
            $data = $this->getContentData();
            $output = $data['totalResults'] === $totalResults;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
        finally {
            $message = "\nAssertion Error:\n" . json_encode($data);
            $this->assertTrue($output, $message);
        }
    }

    /**
     * @Given /^There should be (\d+) pages$/
     * @param int $expectedPages
     */
    public function totalPagesShouldBe(int $expectedPages)
    {
        $output = false;
        $data = [];

        try {
            $data = $this->getContentData();
            $output = $data['totalPages'] === $expectedPages;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
        finally {
            $message = "\nAssertion Error:\n" . json_encode($data);
            $this->assertTrue($output, $message);
        }
    }

    /**
     * @Given /^Page (\d+) should have (\d+) results$/
     * @param int $page
     * @param int $expectedResults
     * @internal param int $expectedPages
     */
    public function pageContainsResults(int $page, int $expectedResults)
    {
        $output = false;
        $data = [];

        try {
            $data = $this->getContentData();
            $output = $data['currentPage'] === $page && count($data['results']) === $expectedResults;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
        finally {
            $message = "\nAssertion Error:\n" . json_encode($data);
            $this->assertTrue($output, $message);
        }
    }

    /**
     * @Given /^Total search requests should increase by (\d+)/
     * @param int $rate
     * @internal param int $expectedPages
     */
    public function searchRequestsIncremented(int $rate)
    {
        $output = false;

        try {
            $totalSearchRequests = $this->searchService->getTotalSearchRequests();
            $diff = $this->totalSearchRequests - $totalSearchRequests;
            $output = $diff === $rate;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
        finally {
            $message = "\nAssertion Error: " . $this->totalSearchRequests . ' === ' . $totalSearchRequests;
            $this->assertTrue($output, $message);
        }
    }
}
