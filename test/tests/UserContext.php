<?php

/**
 * Defines application features from the API context.
 */
class UserContext extends TestContext
{
    const URI_NOT_FUND = 'URI_NOT_FUND';
    const URI_SEARCH = 'URI_SEARCH';
    const URI_REGISTER = 'URI_REGISTER';
    const URI_FAVORITE = 'URI_FAVORITE';
    const URI_TERMS_MOST_USED = 'URI_TERMS_MOST_USED';

    private $uriNotFound;
    private $httpResponse;
    private $user;
    private $userEmail;
    private $userPassword;
    private $searchService;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->uriNotFound = env(self::URI_NOT_FUND, '/not-found');
        $this->httpResponse = new \Illuminate\Http\Response();
        $this->searchService = $this->app->make('App\Services\GifSearch');
    }

    /**
     * @return array
     */
    private function getContentData()
    {
        $output = [];

        try {
            $content = json_decode($this->httpResponse->content(), true);
            $output = $content['data'];
        }
        catch (\Exception $exception) {
        }
        finally {
            return $output;
        }
    }

    /**
     * @Given /^User e-mail is "([^"]*)"$/
     * @param string $email
     */
    public function setUserEmail(string $email)
    {
        try {
            $this->userEmail = $email;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^User password is "([^"]*)"$/
     * @param string $password
     */
    public function setUserPassword(string $password)
    {
        try {
            $this->userPassword = $password;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @When User tries to register
     */
    public function userRegister()
    {
        try {
            $apiUri = env(self::URI_REGISTER, $this->uriNotFound);

            $this->httpResponse = $this
                ->json('POST', $apiUri, [
                    'email' => $this->userEmail,
                    'password' => $this->userPassword,
                ])
                ->response;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }

    /**
     * @Given /^User starts session as "([^"]*)" with password "([^"]*)"$/
     * @param string $email
     * @param string $password
     */
    public function startSession(string $email, string $password)
    {
        $this->userEmail = $email;
    }

    /**
     * @When /^User favourites GIF\-ID (\d+)$/
     * @param int $imageId
     */
    public function userFavoriteGif(int $imageId)
    {
        try {
            $apiUri = env(self::URI_FAVORITE, $this->uriNotFound);

            $this->httpResponse = $this
                ->json('POST', $apiUri, [
                    'email' => $this->userEmail,
                    'image' => $imageId,
                ])
                ->response;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }

    /**
     * @Then Consumer gets a :expectedHttpCode response
     * @param $expectedHttpCode
     */
    public function consumerGetsHttpResponse(int $expectedHttpCode)
    {
        $output = false;
        $content = [];

        try {
            $status = $this->httpResponse->status();
            $content = json_decode($this->httpResponse->content(), true);
            $output = $status === $expectedHttpCode;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
        finally {
            $message = "\nAssertion Error:\n" . json_encode($content);
            $this->assertTrue($output, $message);
        }
    }
}
